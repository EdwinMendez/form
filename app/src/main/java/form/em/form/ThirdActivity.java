package form.em.form;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemSelected;

public class ThirdActivity extends AppCompatActivity {

     //@BindView(R.id.university_spinner)
     Spinner universitySpinner;

     Spinner careerSpinner;
     Spinner semestersSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);
        ButterKnife.bind(this);

        universitySpinner = (Spinner) findViewById(R.id.university_spinner);
        careerSpinner = (Spinner) findViewById(R.id.career_spinner);
        semestersSpinner = (Spinner) findViewById(R.id.semesters_spinner);


        final ArrayAdapter<CharSequence> adapterUniversities = ArrayAdapter.createFromResource(this,
                R.array.univerties, android.R.layout.simple_spinner_item);

        universitySpinner.setAdapter(adapterUniversities);

        universitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int iUniversity, long l) {

                int[] careerArray = {R.array.career_unac,
                                     R.array.career_eafit,
                                     R.array.career_uco};
                final ArrayAdapter<CharSequence> adapterCareer = ArrayAdapter.createFromResource(
                        adapterView.getContext(),
                        careerArray[iUniversity],
                        android.R.layout.simple_spinner_item);


                careerSpinner.setAdapter(adapterCareer);
                careerSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        int[] semestreArray = new int[2];

                        if(universitySpinner.getSelectedItemId() == 0){
                            semestreArray[0] = R.array.accountancy_semesters_unac;
                            semestreArray[1] = R.array.enginering_semesters_unac;
                        } else if(universitySpinner.getSelectedItemId() == 1){
                            semestreArray[0] = R.array.accountancy_semesters_eafit;
                            semestreArray[1] = R.array.enginering_semesters_eafit;
                        } else if(universitySpinner.getSelectedItemId() == 2){
                            semestreArray[0] = R.array.accountancy_semesters_uco;
                            semestreArray[1] = R.array.enginering_semesters_uco;
                        }

                        final ArrayAdapter<CharSequence> adapterSemester = ArrayAdapter.createFromResource(
                                adapterView.getContext(),
                                semestreArray[i],
                                android.R.layout.simple_spinner_item);
                        semestersSpinner.setAdapter(adapterSemester);

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) { }
        });

    }

    @OnItemSelected({R.id.university_spinner})
    public void see(){
        Toast.makeText(getApplicationContext(), "Edwin", Toast.LENGTH_LONG).show();
    }
}
