package form.em.form;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class SecondActivity extends AppCompatActivity {

    @BindView(R.id.guardarButton)
    public Button guardar_Button;

    @BindView(R.id.nombreEditText)
    public EditText nombre;

    @BindView(R.id.apellidoEditText)
    public EditText apellido;

    @BindView(R.id.sexoRadioGroup)
    public RadioGroup sexo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        ButterKnife.bind(this);
    }

    public void initComponent(){

    }

    @OnClick(R.id.guardarButton)
    public void guardar(View view){

        if(nombre.getText().toString().trim().equalsIgnoreCase("")){
            nombre.setError(getString(R.string.error_nombre));
        }

        if(apellido.getText().toString().trim().equalsIgnoreCase("")){
            apellido.setError(getString(R.string.error_apellido));
        }

    }


    @OnCheckedChanged({R.id.masculinoRadioButton, R.id.femeninoRadioButton})
    public void sexo(CompoundButton button, boolean checked){
        if(checked){
            switch (button.getId()){
                case R.id.masculinoRadioButton:
                    Toast.makeText(this, "MASCULINO", Toast.LENGTH_LONG).show();
                    break;
                case R.id.femeninoRadioButton:
                    Toast.makeText(this, "FEMENINO", Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

}
